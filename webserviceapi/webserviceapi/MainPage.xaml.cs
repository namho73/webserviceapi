﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Xamarin.Forms;
using Plugin.Connectivity;



namespace webserviceapi
{
	public partial class MainPage : ContentPage
	{
		public MainPage()
		{
			InitializeComponent();
		}

        async void Handle_GetWordTranslation(object sender, System.EventArgs e)
        {
            if (wordEntry.Text == "")
            {
                return;
            }

            if (!IHaveInternet())
            {
                DisplayAlert("No Internet","No internet connection detected", "OK");
                return;
            }
            if (wordEntry.Text.Length > 0)
            {


                DictionaryModel translatorData = new DictionaryModel();
                var client = new HttpClient();
                var dictApiAddress = "https://owlbot.info/api/v2/dictionary/" + wordEntry.Text.ToLower() + "?format=json";
                var uri = new Uri(dictApiAddress);
                var response = await client.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    var jsonContent = await response.Content.ReadAsStringAsync();
                    jsonContent = "{\"Terms\":" + jsonContent + "}";
                    translatorData = JsonConvert.DeserializeObject<DictionaryModel>(jsonContent);
                }

                dictListView.ItemsSource = new ObservableCollection<SingleTerm>(translatorData.Terms);
                if (translatorData.Terms.Count == 0)
                {
                    lbl.Text = "Missing or not a word";
                }
                else
                {
                    lbl.Text = "";
                }
            }
            
        }

        public bool IHaveInternet()
        {
            if (!CrossConnectivity.IsSupported)
                return true;

            //Do this only if you need to and aren't listening to any other events as they will not fire.
            var connectivity = CrossConnectivity.Current;

            try
            {
                return connectivity.IsConnected;
            }
            finally
            {
                CrossConnectivity.Dispose();
            }
        }
    }
}
