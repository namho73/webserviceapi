﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace webserviceapi
{
    class DictionaryModel
    {
       
        public List<SingleTerm> Terms { get; set; }
    }

    public class SingleTerm
    {
        public string type { get; set; }
        public string definition { get; set; }
        public string example { get; set; }
    }
}
